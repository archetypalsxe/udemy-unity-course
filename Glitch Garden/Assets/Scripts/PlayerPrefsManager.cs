﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerPrefsManager : MonoBehaviour {

    const string MASTER_VOLUME_KEY = "master_volume";
    const string DIFFICULTY_KEY = "difficulty";
    // level_unlocked_2 = false; (2nd level is locked)
    const string LEVEL_KEY = "level_unlocked_";

    public static void SetMasterVolume (int volume) {
        SetMasterVolume((float)volume);
    }

    public static void SetMasterVolume (float volume) {
        if (volume >= 0f && volume <= 1f) {
            PlayerPrefs.SetFloat (MASTER_VOLUME_KEY, volume);
        } else{
            Debug.LogError("Volume was not valid "+ volume);
        }
    }

    public static float GetMasterVolume() {
        return PlayerPrefs.GetFloat (MASTER_VOLUME_KEY);
    }

    public static void UnlockLevel (int level) {
        if (level <= SceneManager.sceneCountInBuildSettings - 1) {
            PlayerPrefs.SetInt (LEVEL_KEY + level.ToString(), 1);
        } else {
            Debug.LogError("Invalid level unlock requested: "+ level.ToString());
        }

    }

    public static bool IsLevelUnlocked (int level) {
        if (level <= SceneManager.sceneCountInBuildSettings - 1) {
            return PlayerPrefs.GetInt (LEVEL_KEY + level.ToString()) > 0;
        } else {
            Debug.LogError("Invalid level unlock requested: "+ level.ToString());
            return false;
        }
    }

    public static void SetDifficulty (float difficulty) {
        if (difficulty >= 1f && difficulty <= 3f) {
            PlayerPrefs.SetFloat(DIFFICULTY_KEY, difficulty);
        } else {
            Debug.LogError("Difficulty out of range: "+ difficulty.ToString());
        }
    }

    public static float GetDifficulty () {
        return PlayerPrefs.GetFloat(DIFFICULTY_KEY);
    }
}
