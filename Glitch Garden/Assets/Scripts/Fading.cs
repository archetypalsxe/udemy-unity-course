﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fading : MonoBehaviour {

	public float fadeInSeconds;

	private Image fadePanel;
	private Color currentColor;

	void Start () {
		this.fadePanel = GetComponent<Image>();
		this.currentColor = fadePanel.color;
	}

	void Update () {
		if(Time.timeSinceLevelLoad < this.fadeInSeconds) {
            this.currentColor.a = 1 - (Time.timeSinceLevelLoad / this.fadeInSeconds);
            this.fadePanel.color = this.currentColor;
		} else {
			gameObject.SetActive(false);
		}
	}
}
