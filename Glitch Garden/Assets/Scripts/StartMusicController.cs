﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMusicController : MonoBehaviour {

	protected AudioSource music;

    void Awake() {
        music = GetComponent<AudioSource>();
        music.volume = PlayerPrefsManager.GetMasterVolume();
        Debug.Log(music.volume);
    }

    void Update() {
        Debug.Log(music.volume);
    }
}
