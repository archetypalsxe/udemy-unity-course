﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsController : MonoBehaviour {

    protected const float DEFAULT_VOLUME = 0.8f;
    protected const float DEFAULT_DIFFICULTY = 2f;

    public Slider volumeSlider, difficultySlider;
    public LevelManager levelManager;

    private MusicManager musicManager;

    public void SaveAndExit() {
        PlayerPrefsManager.SetMasterVolume (volumeSlider.value);
        PlayerPrefsManager.SetDifficulty (difficultySlider.value);
        levelManager.LoadLevel("Start");
    }

    public void SetDefaults() {
        volumeSlider.value = DEFAULT_VOLUME;
        difficultySlider.value = DEFAULT_DIFFICULTY;
    }

	void Start () {
		musicManager = GameObject.FindObjectOfType<MusicManager>();
        volumeSlider.value = PlayerPrefsManager.GetMasterVolume();
        difficultySlider.value = PlayerPrefsManager.GetDifficulty();
	}
	
	void Update () {
        if(musicManager) {
            musicManager.SetVolume(volumeSlider.value);
        }
	}
}
