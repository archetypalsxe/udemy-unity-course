﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour {

	protected AudioSource music;
	protected static MusicManager instance = null;

    public void SetVolume(float volume) {
        music.volume = volume;
    }

	void Awake() {
		if(instance != null && instance != this) {
			Destroy(gameObject);
		} else {
			instance = this;
			GameObject.DontDestroyOnLoad(gameObject);
			this.music = GetComponent<AudioSource>();
		}
	}

	// Update is called once per frame
	void Update () {
	}

	protected void OnEnable() {
		SceneManager.sceneLoaded += PlayLevelMusic;
	}

	protected void OnDisable() {
		SceneManager.sceneLoaded -= PlayLevelMusic;
	}

	protected void PlayLevelMusic (Scene level, LoadSceneMode loadSceneMode) {
		GameObject[] musicSources = GameObject.FindGameObjectsWithTag("Music");
		if(musicSources.Length > 0) {
			if(this.music) {
				this.music.Stop();
			}
			AudioSource audioSource = musicSources[0].GetComponent<AudioSource>();
			this.music.clip = audioSource.clip;
			this.music.loop = audioSource.loop;
			this.music.Play();
		}
	}
}
