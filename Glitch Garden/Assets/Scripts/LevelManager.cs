﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	public float autoLoadNextLevelAfter;

	void Start() {
		if(autoLoadNextLevelAfter > 0) {
			Invoke("LoadNextLevel", this.autoLoadNextLevelAfter);
		}
	}

	// Load up the requested level
	public void LoadLevel(string name) {
		SceneManager.LoadScene(name);
	}

	// Quit the game
	public void ExitGame() {
		Debug.Log("Requested has been recevied to exit the game");
		Application.Quit();
	}

	public void LoadNextLevel() {
		SceneManager.LoadScene(SceneManager.GetSceneAt(0).buildIndex + 1);
	}

}
