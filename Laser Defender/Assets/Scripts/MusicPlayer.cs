﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicPlayer : MonoBehaviour {

	public AudioClip startClip;
	public AudioClip gameClip;
	public AudioClip endClip;

	protected static MusicPlayer instance = null;
	protected AudioSource music;

	void Awake() {
		if(instance != null && instance != this) {
			Destroy(gameObject);
		} else {
			instance = this;
			GameObject.DontDestroyOnLoad(gameObject);
			this.music = GetComponent<AudioSource>();
			music.clip = startClip;
			music.loop = true;
			music.Play();
		}
	}

	protected void OnEnable() {
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	protected void OnDisable() {
		SceneManager.sceneLoaded -= OnSceneLoaded;
	}

	protected void OnSceneLoaded (Scene level, LoadSceneMode loadSceneMode) {
		if(this.music) {
			this.music.Stop();
		} else {
			this.music = GetComponent<AudioSource>();
		}
		int levelNumber = level.buildIndex;
		if(levelNumber == 0) {
			this.music.clip = this.startClip;
		} else if (levelNumber == 3) {
			music.clip = this.gameClip;
		} else {
			music.clip = this.endClip;
		}

		music.loop = true;
		music.Play();
	}
}
