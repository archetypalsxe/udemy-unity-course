﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

	protected static Text text;
	protected static int score = 0;
	protected static string prefix = "Score: ";

	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();
		Reset();
		setText();
	}

	public static int GetScore() {
		return score;
	}

	public static void Reset() {
		score = 0;
	}

	public void ScorePoints(int points) {
		score += points;
		setText();
	}

	protected static void setText() {
		text.text = prefix + score;
	}
}
